package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.dao.PortDAO;
import com.example.demo.mailservice.Email;
import com.example.demo.model.Port;
import com.example.demo.status.Status;

@Controller
public class PortController {
	@Autowired
	PortDAO pd;
	
	@Autowired
	@Qualifier("email")
	private Email senderService;
	
	@Autowired
	@Qualifier("sts")
	private Status status;
	
	
	@RequestMapping("status")
	public String status() {
		return "sts";
	}
	
	
	@RequestMapping("/")
	public String Home() {
		return "Home.jsp";
	}
	
	@RequestMapping("addip")
	@ResponseBody
	public String index(Port p) {
		pd.save(p);

		return "Added Successfully";
	}
	
	@RequestMapping("getip")
	@ResponseBody
	public Optional<Port> getip( int id) {
		 return pd.findById(id);
	}
	
	@RequestMapping("port")
	@ResponseBody
	public List<Port> getall() {
		 return pd.findAll();
	}

	@RequestMapping("delete")
	@ResponseBody
	public String delete( int id) {
		 pd.deleteById(id);
		 return "deleted successfully";
	}
	
	
//	@RequestMapping("/port/{id}")
//	@ResponseBody
//	public Optional<Port> getid(@PathVariable("id") int id) {
//		return pd.findById(id);
//	}
	
	@RequestMapping("sts")
	@ResponseBody
	public String Sts(int port) {
		
		String str=status.isRemotePortInUse("localhost",port);
		return str;
	}
	
	@RequestMapping("status/{id}")
	@ResponseBody
	public String Status(@PathVariable("id")int id) {
		int portnumber=id;
		String s=String.valueOf(portnumber);
		String str=status.isRemotePortInUse("localhost",id);
		String s1="port "+s+":"+str;
		senderService.sendEmail("subramani832ak@gmail.com","Port status" , s1);
		return str;
	}

}
