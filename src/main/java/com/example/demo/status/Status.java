package com.example.demo.status;

import java.io.IOException;
import java.net.Socket;

import org.springframework.stereotype.Component;

@Component("sts")
public class Status {
	public static String isRemotePortInUse(String hostName,int portNumber) {
		try {
			new Socket(hostName,portNumber).close();
			
			return "The port is active";
		} catch(IOException e) {
			return "The port is inactive";
			
		}
	
	}
}
