package com.example.demo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity

public class Port {

@Id
@Column(name="id")
private int id;

@Column(name="port_number")
private int port_number;



public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public int getPort_number() {
	return port_number;
}

public void setPort_number(int port_number) {
	this.port_number = port_number;
}

public Port() {
	super();
}

public Port(int id, int port_number) {
	super();
	this.id = id;
	this.port_number = port_number;
}


}
